package com.du.shankar.syllabus;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;



import java.util.ArrayList;

public class SemesterActivity extends AppCompatActivity {
    String subs[][];
    int n;
    int mBranch;
    int id, q;
    TypedArray ta, array;
    TextView suph;
    ListView listView;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    SubjectAdapter adapter;
    String sb = "";
    ArrayList<Subject> subjects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_semester);
        //Bundle extras=getIntent().getExtras();
        pref = getApplication().getSharedPreferences("Options", MODE_PRIVATE);
        editor = pref.edit();
        int x = pref.getInt("Sem", 0);
        mBranch = pref.getInt("Batch", 0);
        if(x==1)
        {
            mBranch=0;
            setTitle("Semester 1 & 2");
        }
        else{
            setTitle("Semester " + x);
        }

        /*int x=extras.getInt("Sem");
        mBranch=extras.getInt("Batch");*/

        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayUseLogoEnabled(false);


        listView = (ListView) findViewById(R.id.listSubjects);


        subjects = new ArrayList<Subject>();


        suph = (TextView) findViewById(R.id.phSemText);
        switch (x) {
            case 1:
                array=getResources().obtainTypedArray(R.array.s1branches);
                list();
                //listView.setVisibility(View.GONE);

                break;
            case 3:
                array = getResources().obtainTypedArray(R.array.s3branches);
                list();
                break;

            case 4:
                array = getResources().obtainTypedArray(R.array.s4branches);
                list();
                break;
            case 5:
                //array=getResources().obtainTypedArray(R.array.s5branches);
                //list();

                listView.setVisibility(View.GONE);
                break;
            case 6:
                //array=getResources().obtainTypedArray(R.array.s6branches);
                //list();
                listView.setVisibility(View.GONE);
                break;
            case 7:
                //array=getResources().obtainTypedArray(R.array.s7branches);
                //list();
                listView.setVisibility(View.GONE);
                break;
            case 8:
                //array = getResources().obtainTypedArray(R.array.s8branches);
                //list();
                listView.setVisibility(View.GONE);
                break;
        }
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                makeSubjectString(position);
                Intent n=new Intent(SemesterActivity.this, SubjectActivity.class);
                startActivity(n);
                finish();
            }
        });
    }
    public void list() {
        q = array.getResourceId(mBranch, 0);
        ta = getResources().obtainTypedArray(q);
        array.recycle();
        suph.setVisibility(View.GONE);
        n = ta.length();
        subs=new String[n][];
        for (int i = 0; i < n; i++) {
            id = ta.getResourceId(i, 0);
            if (id > 0) {
                subjects.add(new Subject(getResources().getStringArray(id)));
                subs[i] = getResources().getStringArray(id);
            }
        }
        adapter = new SubjectAdapter(this, subjects);
        listView.setAdapter(adapter);
        ta.recycle();
    }

    public void makeSubjectString(int k) {
        for (int i = 0; i < subs[k].length; i++) {
            sb += (subs[k][i] + "/blah/");
        }
        editor.putString("Sub", sb);
        editor.commit();
    }

}
